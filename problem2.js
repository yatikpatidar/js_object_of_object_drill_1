// Q2 Find all users staying in Germany.

function usersLocation(usersData, inputLocation) {

    if (typeof (usersData) === 'object') {

        let result = [] ;

        for (let key in usersData) {
            if (usersData[key]['nationality'] === inputLocation) {
                result.push(key)
            }
        }

        return result;

    } else {
        return []
    }
}

module.exports = usersLocation