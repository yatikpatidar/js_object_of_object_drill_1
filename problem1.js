
// Q1 Find all users who are interested in playing video games.

function usersInterest(usersData, givenInterest) {

    if (typeof (usersData) === 'object') {

        let result = []

        for (let key in usersData) {

            const innerObject = usersData[key];

            let interestKey;

            if (innerObject['interests'] != undefined) {
                interestKey = 'interests';
            }
            else {
                interestKey = 'interest';
            }

            const interestList= innerObject[interestKey][0].split(",");

            for (let index = 0; index < interestList.length; index++) {
                if (interestList[index].toLowerCase().includes(givenInterest.toLowerCase())) {
                    result.push(key)
                    break
                }
            }
        }

        return result
    }
    else {
        return []
    }
}

module.exports = usersInterest