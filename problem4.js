// Q4 Group users based on their Programming language mentioned in their designation.

function usersGroup(usersData) {

    if (typeof (usersData) === 'object') {

        const commonProgrammingLanguage = ['Python', 'Javascript', 'Java', 'C++', 'Golang', 'PHP', 'C', 'Go', 'Kotlin', 'Rust', 'Ruby']

        let result = {}
        for (let key in usersData) {

            const desginationList = usersData[key]['desgination'].split(" ")

            const desginationListSize = desginationList.length;

            for (let desginationListIndex = 0; desginationListIndex < desginationListSize; desginationListIndex++) {

                if (commonProgrammingLanguage.includes(desginationList[desginationListIndex])) {

                    if (result[desginationList[desginationListIndex]] != undefined) {
                        result[desginationList[desginationListIndex]].push(key)
                    } else {
                        result[desginationList[desginationListIndex]] = [key]
                    }

                }
            }

        }
        return result
    } else {
        return []
    }

}

module.exports = usersGroup