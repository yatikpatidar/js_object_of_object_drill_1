// Q3 Find all users with masters Degree.


function usersListAccToQualification(usersData, inputQualification) {

    if (typeof (usersData) === 'object') {

        let result = []
        for (let key in usersData) {
            if (usersData[key]['qualification'].includes(inputQualification)) {
                result.push(key)
            }
        }
        return result;

    } else {
        return []
    }
}

module.exports = usersListAccToQualification