// Q3 Find all users with masters Degree.


const users = require('../data')

const usersListAccToQualification = require('../problem3')

userList = usersListAccToQualification(users, "Masters")

if (userList != []) {
    console.log("list of user staying in Germany ", userList)

} else {
    console.log("Check before passing data as an argument")
}