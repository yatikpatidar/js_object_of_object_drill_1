
// Q1 Find all users who are interested in playing video games.

const users = require('../data')

const usersInterest = require('../problem1')

userList = usersInterest(users , 'Video game')

if(userList != []){
    console.log("list of user who are interested in video games " , userList)

}else{
    console.log("Check before passing data as an argument")
}
