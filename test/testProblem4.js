// Q4 Group users based on their Programming language mentioned in their designation.


const users = require('../data')

const usersGroup = require('../problem4')

userList = usersGroup(users)

if (userList != []) {
    console.log("Group users based on their Programming language ", userList)

} else {
    console.log("Check before passing data as an argument")
}
